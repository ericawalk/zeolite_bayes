#!/usr/bin/env python
# coding: utf-8

# In[1]:

import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D 
import scipy
from scipy.stats import multivariate_normal
from scipy.integrate import odeint
import pandas as pd
import seaborn as sns
import pickle

class ip:
    # Q (for proposal samples).  
    # Initialize experimental data.
    mcmc_length = 50000
    mcmc_burn_in = 10000 # number of samples
    # One ethylene, One water, xxxWater to Ethylene Ads, Two ethylene, Two water, Ethylene to Water Ads 
    mu_prior = np.array([0.18339995, -0.2511143, 0.24907733, 0.35008913, 0.11494823])
    cov_prior = np.array([[0.16659867, 0.12583594, 0.14927238, 0.09301179, 0.12427158],
                          [0.12583594, 0.09704397, 0.1147893,  0.0704613,  0.09563019],
                          [0.14927238, 0.1147893,  0.14853634, 0.08964314, 0.12541054],
                          [0.09301179, 0.0704613,  0.08964314, 0.05514175, 0.07562473],
                          [0.12427158, 0.09563019, 0.12541054, 0.07562473, 0.10668641]])
    experiment = np.array([1, 0.776, 0.611, 0.447, 0.402])   #5 long
    Q_mu = np.array([0,0,0,0,0])
    Q_cov = cov_prior
    #main function to get samples
    def MetropolisHastings(self):
        samples = np.zeros((self.mcmc_length,5)) 
        samples[0,:] = self.mu_prior #initialize the chain
        likelihoods_vec = np.zeros((self.mcmc_length,5))
        posteriors_un_normed_vec = np.zeros((self.mcmc_length,1))
        priors_vec = np.zeros((self.mcmc_length,1))
        for i in range(1,self.mcmc_length):
            proposal_sample = samples[i-1,:] + np.random.multivariate_normal(self.Q_mu,self.Q_cov)
            prior_proposal = self.prior(proposal_sample)
            likelihood_proposal = self.likelihood(proposal_sample)
            prior_current_location = self.prior(samples[i-1,:])
            likelihood_current_location = self.likelihood(samples[i-1,:])
            accept_pro = (likelihood_proposal*prior_proposal)/(likelihood_current_location*prior_current_location)
            uni_rand = np.random.uniform()
            if uni_rand<accept_pro:
                samples[i,:] = proposal_sample
                posteriors_un_normed_vec[i] = likelihood_proposal*prior_proposal
                likelihoods_vec[i] = likelihood_proposal
                priors_vec[i] = prior_proposal
            else:
                samples[i,:] = samples[i-1,:]
                posteriors_un_normed_vec[i] = likelihood_current_location*prior_current_location
                likelihoods_vec[i] = likelihood_current_location
                priors_vec[i] = prior_current_location
            ########################################
        samples = samples[self.mcmc_burn_in:]
        posteriors_un_normed_vec = posteriors_un_normed_vec[self.mcmc_burn_in:]
        likelihoods_vec = likelihoods_vec[self.mcmc_burn_in:]
        priors_vec = priors_vec[self.mcmc_burn_in:]
        #posterior probabilites are transformed to a standard normal (std=1)
        #for obtaining the evidence:
        evidence = np.mean(posteriors_un_normed_vec)*np.sqrt(2*np.pi*np.std(samples)**2)
        posteriors_vec = posteriors_un_normed_vec/evidence
        log_ratios = np.log(posteriors_vec/priors_vec)
        log_ratios[np.isinf(log_ratios)] = 0
        log_ratios = np.nan_to_num(log_ratios)
        info_gain = np.mean(log_ratios)
        return [evidence, info_gain, samples]
    
    def prior(self,sample):
        probability = multivariate_normal.pdf(x=sample, mean=self.mu_prior, cov=self.cov_prior, allow_singular=False)
        return probability
        
    def likelihood(self,sample):
        c2h4_totalarray = np.zeros(5)
        c2h4_adsarray = np.zeros(5)
        for j,C in enumerate(np.array(concentrations)):
            sol = odeint(microkinetic_model, y0, t, args=(C, H2O_gas_frac, f1, f2, sample[0], sample[1], -0.3195661, sample[2], sample[3], sample[4]))
            c2h4_1Frac = sol[-1,0]
            c2h4_2Frac = sol[-1,2]
            c2h4_2Frac2 = 2.0*c2h4_2Frac
            c2h4_total = c2h4_1Frac+c2h4_2Frac2
            c2h4_totalarray[j] = c2h4_total
            print(C)
            print(c2h4_totalarray)
        c2h4_adsarray = c2h4_totalarray[:]/c2h4_totalarray[0]  
        probability_ra = multivariate_normal.pdf(x=c2h4_adsarray,mean=experiment, cov=0.1)  #0.005
        return probability_ra
#-------------------------------------------------------------------------------------------
#     Microkinetic Model Function
concentrations = np.array([9e-4, 6e-4, 4.5e-4, 3e-4, 2e-4])
experiment = np.array([1, 0.776, 0.611, 0.447, 0.402])
y0 = y=np.array([0, 0, 0, 0, 0 , 1])
t = np.linspace(0,1E10,1E4)
f1 = 6.2014895E5 #adsorption rate constant ethylene on zeolite from collision theory
f2 = 7.7382354E5 #adsorption rate water from collision theory
H2O_gas_frac = 0.06   
kB = 8.617E-5
T=298.15 
def microkinetic_model(y, t, C, H2O_gas_frac, f1, f2, G_1, G_2, G_3, G_4, G_5, G_6):
        theta_C2H4, theta_H2O, theta_2C2H4, theta_2H2O, theta_C2H4H2O, theta_vacant = y
        K_1 = np.exp(-(G_1)/(kB*T))
        K_2 = np.exp(-(G_2)/(kB*T))
        K_3 = np.exp(-(G_3)/(kB*T))
        K_4 = np.exp(-(G_4)/(kB*T))
        K_5 = np.exp(-(G_5)/(kB*T))
        K_6 = np.exp(-(G_6)/(kB*T))
        kr1 = f1/K_1
        kr2 = f2/K_2
        kr3 = f2/K_3       #water adsorbed, C2H4 already adsorbed    
        kr4 = f1/K_4       #ethylene adsorbed, C2H4 already adsorbed 
        kr5 = f2/K_5       #water adsorbed, H2O already adsorbed
        kr6 = f1/K_6       #ethylene adsorbed, H2O already adsorbed
        r_rxn1 = f1*theta_vacant*C - kr1*theta_C2H4
        r_rxn2 = f2*theta_vacant*H2O_gas_frac - kr2*theta_H2O
        r_rxn3 = f2*theta_vacant*theta_C2H4*H2O_gas_frac - kr3*theta_C2H4H2O
        r_rxn4 = f1*theta_vacant*theta_C2H4*C - kr4*theta_2C2H4
        r_rxn5 = f2*theta_vacant*theta_H2O*H2O_gas_frac - kr5*theta_2H2O
        r_rxn6 = f1*theta_vacant*theta_H2O*C - kr6*theta_C2H4H2O
        d_theta_C2H4_dt = r_rxn1 - r_rxn3 - r_rxn4
        d_theta_H2O_dt = r_rxn2 - r_rxn5 - r_rxn6
        d_theta_2C2H4_dt = r_rxn4
        d_theta_2H2O_dt = r_rxn5
        d_theta_C2H4H2O_dt = r_rxn3 + r_rxn6
        d_theta_vacant_dt = -r_rxn1 - r_rxn2 - r_rxn3 - r_rxn4 - r_rxn5 - r_rxn6
        dydt = [d_theta_C2H4_dt, d_theta_H2O_dt, d_theta_2C2H4_dt, d_theta_2H2O_dt, d_theta_C2H4H2O_dt, d_theta_vacant_dt]
        return dydt

#----------------------------------------------------------------------------------
#--------------------Finding Priors and Posteriors---------------------


#calculating prior adsorption fractions
mu_prior = np.array([0.18339995, -0.2511143, -0.3195661, 0.24907733, 0.35008913, 0.11494823])
cov_prior = np.array([[0.16659867, 0.12583594, 0.08350885, 0.14927238, 0.09301179, 0.12427158],
                      [0.12583594, 0.09704397, 0.06683822, 0.1147893,  0.0704613,  0.09563019],
                      [0.08350885, 0.06683822, 0.0613744,  0.09092746, 0.05307424, 0.07804502],
                      [0.14927238, 0.1147893,  0.09092746, 0.14853634, 0.08964314, 0.12541054],
                      [0.09301179, 0.0704613,  0.05307424, 0.08964314, 0.05514175, 0.07562473],
                      [0.12427158, 0.09563019, 0.07804502, 0.12541054, 0.07562473, 0.10668641]])
G_prior=np.random.multivariate_normal(mu_prior, cov_prior, 40000)
theta_C2H4_prior=np.zeros((40000, 5))
theta_H2O_prior=np.zeros((40000,5))
theta_2C2H4_prior=np.zeros((40000,5))
theta_2H2O_prior=np.zeros((40000,5))
theta_C2H4H2O_prior=np.zeros((40000,5))
theta_vacant_prior=np.zeros((40000,5))
for j,C in enumerate(np.array([9e-4, 6e-4, 4.5e-4, 3e-4, 2e-4])):
    print('concentration: ',C)
    for i in range(G_prior.shape[0]):    #shape[0] is 2000
        sol = odeint(microkinetic_model, y0, t, args=(C, H2O_gas_frac, f1, f2, G_prior[i,0], G_prior[i,1], G_prior[i,2], G_prior[i,3], G_prior[i,4], G_prior[i,5]))
        theta_C2H4_prior[i,j] = sol[-1,0]
        theta_H2O_prior[i,j] = sol[-1,1]
        theta_2C2H4_prior[i,j] = sol[-1,2]
        theta_2H2O_prior[i,j] = sol[-1,3]
        theta_C2H4H2O_prior[i,j] = sol[-1,4]
        theta_vacant_prior[i,j] = sol[-1,5]
#graphing prior ads frac when specific concentration
Cp = theta_C2H4_prior[:,0]
Hp = theta_H2O_prior[:,0]
C2p = theta_2C2H4_prior[:,0]
H2p = theta_2H2O_prior[:,0]
CHp = theta_C2H4H2O_prior[:,0]
Vp = theta_vacant_prior[:,0]
Cp = np.reshape(Cp, (40000,1))
Hp = np.reshape(Hp, (40000,1))
C2p = np.reshape(C2p, (40000,1))
H2p = np.reshape(H2p, (40000,1))
CHp = np.reshape(CHp, (40000,1))
Vp = np.reshape(Vp, (40000,1))
x1 = np.hstack((Cp,Hp))
x2 = np.hstack((x1,C2p))
x3 = np.hstack((x2,H2p))
x4 = np.hstack((x3,CHp))
x = np.hstack((x4,Vp))
x=np.reshape(x, (40000,6))
preset_bins = 10
fig, ax = plt.subplots()
ax.hist(x, bins=preset_bins, label=[r'$\theta_{C2H4}$',r'$\theta_{H2O}$',r'$\theta_{2 C2H4}$',r'$\theta_{2 H2O}$', r'$\theta_{C2H4+H2O}$', r'$\theta_{vacant}$'])
ax.set_title('Prior Adsorption Fractions',  fontsize=18)
ax.set_ylabel('Frequency', fontsize=18)
ax.set_xlabel('Surface Fraction', fontsize=18)
ax.set_xlim(-.02,1.02)
ax.set_ylim(0,40000)
ax.legend(loc='best', fontsize=16)
fig.savefig('PriorAdsFrac900ppm_HSSZ13.png', dpi=220)
pickle.dump(x, open("PriorAdsFrac_HSSZ13_900.p", "wb"))

Cp2 = theta_C2H4_prior[:,1]
Hp2 = theta_H2O_prior[:,1]
C2p2 = theta_2C2H4_prior[:,1]
H2p2 = theta_2H2O_prior[:,1]
CHp2 = theta_C2H4H2O_prior[:,1]
Vp2 = theta_vacant_prior[:,1]
Cp2 = np.reshape(Cp2, (40000,1))
Hp2 = np.reshape(Hp2, (40000,1))
C2p2 = np.reshape(C2p2, (40000,1))
H2p2 = np.reshape(H2p2, (40000,1))
CHp2 = np.reshape(CHp2, (40000,1))
Vp2 = np.reshape(Vp2, (40000,1))
x12 = np.hstack((Cp2,Hp2))
x22 = np.hstack((x12,C2p2))
x32 = np.hstack((x22,H2p2))
x42 = np.hstack((x32,CHp2))
x_2 = np.hstack((x42,Vp2))
x_2=np.reshape(x_2, (40000,6))
pickle.dump(x_2, open("PriorAdsFrac_HSSZ13_600.p", "wb"))

Cp3 = theta_C2H4_prior[:,2]
Hp3 = theta_H2O_prior[:,2]
C2p3 = theta_2C2H4_prior[:,2]
H2p3 = theta_2H2O_prior[:,2]
CHp3 = theta_C2H4H2O_prior[:,2]
Vp3 = theta_vacant_prior[:,2]
Cp3 = np.reshape(Cp3, (40000,1))
Hp3 = np.reshape(Hp3, (40000,1))
C2p3 = np.reshape(C2p3, (40000,1))
H2p3 = np.reshape(H2p3, (40000,1))
CHp3 = np.reshape(CHp3, (40000,1))
Vp3 = np.reshape(Vp3, (40000,1))
x13 = np.hstack((Cp3,Hp3))
x23 = np.hstack((x13,C2p3))
x33 = np.hstack((x23,H2p3))
x43 = np.hstack((x33,CHp3))
x_3 = np.hstack((x43,Vp3))
x_3=np.reshape(x_3, (40000,6))
pickle.dump(x_3, open("PriorAdsFrac_HSSZ13_450.p", "wb"))

Cp4 = theta_C2H4_prior[:,3]
Hp4 = theta_H2O_prior[:,3]
C2p4 = theta_2C2H4_prior[:,3]
H2p4 = theta_2H2O_prior[:,3]
CHp4 = theta_C2H4H2O_prior[:,3]
Vp4 = theta_vacant_prior[:,3]
Cp4 = np.reshape(Cp4, (40000,1))
Hp4 = np.reshape(Hp4, (40000,1))
C2p4 = np.reshape(C2p4, (40000,1))
H2p4 = np.reshape(H2p4, (40000,1))
CHp4 = np.reshape(CHp4, (40000,1))
Vp4 = np.reshape(Vp4, (40000,1))
x14 = np.hstack((Cp4,Hp4))
x24 = np.hstack((x14,C2p4))
x34 = np.hstack((x24,H2p4))
x44 = np.hstack((x34,CHp4))
x_4 = np.hstack((x44,Vp4))
x_4=np.reshape(x_4, (40000,6))
pickle.dump(x_4, open("PriorAdsFrac_HSSZ13_300.p", "wb"))

Cp5 = theta_C2H4_prior[:,4]
Hp5 = theta_H2O_prior[:,4]
C2p5 = theta_2C2H4_prior[:,4]
H2p5 = theta_2H2O_prior[:,4]
CHp5 = theta_C2H4H2O_prior[:,4]
Vp5 = theta_vacant_prior[:,4]
Cp5 = np.reshape(Cp5, (40000,1))
Hp5 = np.reshape(Hp5, (40000,1))
C2p5 = np.reshape(C2p5, (40000,1))
H2p5 = np.reshape(H2p5, (40000,1))
CHp5 = np.reshape(CHp5, (40000,1))
Vp5 = np.reshape(Vp5, (40000,1))
x15 = np.hstack((Cp5,Hp5))
x25 = np.hstack((x15,C2p5))
x35 = np.hstack((x25,H2p5))
x45 = np.hstack((x35,CHp5))
x_5 = np.hstack((x45,Vp5))
x_5=np.reshape(x_5, (40000,6))
pickle.dump(x_5, open("PriorAdsFrac_HSSZ13_200.p", "wb"))

#----------------------------------------------------------------------------------------------------
#calculating posterior adsorption fractions  

evidences=[]
info_gains=[]
all_samples=[]

ip_object = ip()
concentrations = np.array([9e-4, 6e-4, 4.5e-4, 3e-4, 2e-4])
experiment = np.array([1, 0.776, 0.611, 0.447, 0.402])
[evidence, info_gain, samples] = ip_object.MetropolisHastings()
evidences.append(evidence)
info_gains.append(info_gain)
all_samples.append(samples)
print('Evidence: ' + str(evidence))
print('Information Gain: ' + str(info_gain))
print('Samples: ' +str(samples))
print(samples.shape)  
evidencearray = np.asarray(evidences)
print('evidence shape')
print(evidencearray.shape)
np.savetxt('EvidenceH2O_HSSZ13.csv', evidencearray)
# One ethylene, One water, [Water to Ethylene Ads], Two ethylene, Two water, Ethylene to Water Ads
samplearray = np.asarray(all_samples)
print('all samples shape')
print(samplearray.shape)     #(1, 40000, 5)
samplearray = np.reshape(samplearray, (40000,5))

prior_c2h4 = G_prior[:,0]
prior_2c2h4 = G_prior[:,3]
prior_h2o = G_prior[:,1]
prior_2h2o = G_prior[:,4]
prior_ctoh = G_prior[:,5]
prior_htoc = G_prior[:,2]
sample_c2h4 = samplearray[:,0]
sample_2c2h4 = samplearray[:,2]
sample_h2o = samplearray[:,1]
sample_2h2o = samplearray[:,3]
sample_ctoh = samplearray[:,4]

fig_s, ax_s = plt.subplots()
ax_s.hist(prior_c2h4, bins=30, density=True, alpha=0.5, label='prior')
ax_s.hist(sample_c2h4, bins=30, density=True, alpha=0.5, label='posterior')
ax_s.set_title(r'Prior and Posterior $C_2H_4$ Energy',  fontsize=18)
ax_s.set_ylabel('Probability Density', fontsize=18)
ax_s.set_xlabel('Free Energy', fontsize=18)
ax_s.legend(loc='best', fontsize=16)
fig_s.savefig('AdsEng_C2H4_HSSZ13_withH2O.png', dpi=320)

fig_s2, ax_s2 = plt.subplots()
ax_s2.hist(prior_2c2h4, bins=30, density=True, alpha=0.5, label='prior')
ax_s2.hist(sample_2c2h4, bins=30, density=True, alpha=0.5, label='posterior')
ax_s2.set_title(r'Prior and Posterior 2 $C_2H_4$ Energy',  fontsize=18)
ax_s2.set_ylabel('Prabability Density', fontsize=18)
ax_s2.set_xlabel('Free Energy', fontsize=18)
ax_s2.legend(loc='best', fontsize=16)
fig_s2.savefig('AdsEng_2C2H4_HSSZ13_withH2O.png', dpi=320)

fig_s3, ax_s3 = plt.subplots()
ax_s3.hist(prior_h2o, bins=30, density=True, alpha=0.5, label='prior')
ax_s3.hist(sample_h2o, bins=30, density=True, alpha=0.5, label='posterior')
ax_s3.set_title(r'Prior and Posterior $H_2O$ Energy',  fontsize=18)
ax_s3.set_ylabel('Prabability Density', fontsize=18)
ax_s3.set_xlabel('Free Energy', fontsize=18)
ax_s3.legend(loc='best', fontsize=16)
fig_s3.savefig('AdsEng_H2O_HSSZ13_withH2O.png', dpi=320)

fig_s4, ax_s4 = plt.subplots()
ax_s4.hist(prior_2h2o, bins=30, density=True, alpha=0.5, label='prior')
ax_s4.hist(sample_2h2o, bins=30, density=True, alpha=0.5, label='posterior')
ax_s4.set_title(r'Prior and Posterior 2 $H_2O$ Energy',  fontsize=18)
ax_s4.set_ylabel('Prabability Density', fontsize=18)
ax_s4.set_xlabel('Free Energy', fontsize=18)
ax_s4.legend(loc='best', fontsize=16)
fig_s4.savefig('AdsEng_2H2O_HSSZ13_withH2O.png', dpi=320)

fig_s5, ax_s5 = plt.subplots()
ax_s5.hist(prior_ctoh, bins=30, density=True, alpha=0.5, label='prior')
ax_s5.hist(sample_ctoh, bins=30, density=True, alpha=0.5, label='posterior')
ax_s5.set_title(r'Prior and Posterior $C_2H_4$ to $H_2O$ Energy',  fontsize=18)
ax_s5.set_ylabel('Prabability Density', fontsize=18)
ax_s5.set_xlabel('Free Energy', fontsize=18)
ax_s5.legend(loc='best', fontsize=16)
fig_s5.savefig('AdsEng_C2H4toH2O_HSSZ13_withH2O.png', dpi=320)

theta_C2H4_posterior=np.zeros((40000, 5))
theta_H2O_posterior=np.zeros((40000,5))
theta_2C2H4_posterior=np.zeros((40000,5))
theta_2H2O_posterior=np.zeros((40000,5))
theta_C2H4H2O_posterior=np.zeros((40000,5))
theta_vacant_posterior=np.zeros((40000,5))
for j,C in enumerate(np.array([9e-4, 6e-4, 4.5e-4, 3e-4, 2e-4])):
    print('concentration: ',C)
    for i in range(samplearray.shape[0]):    
        sol = odeint(microkinetic_model, y0, t, args=(C, H2O_gas_frac, f1, f2, sample_c2h4[i], sample_h2o[i], -0.3195661, sample_2c2h4[i], sample_2h2o[i], sample_ctoh[i]))
        theta_C2H4_posterior[i,j] = sol[-1,0]
        theta_H2O_posterior[i,j] = sol[-1,1]
        theta_2C2H4_posterior[i,j] = sol[-1,2]
        theta_2H2O_posterior[i,j] = sol[-1,3]
        theta_C2H4H2O_posterior[i,j] = sol[-1,4]
        theta_vacant_posterior[i,j] = sol[-1,5]
#graphing posterior ads frac when specific concentration
Cpost = theta_C2H4_posterior[:,0]
Hpost = theta_H2O_posterior[:,0]
C2post = theta_2C2H4_posterior[:,0]
H2post = theta_2H2O_posterior[:,0]
CHpost = theta_C2H4H2O_posterior[:,0]
Vpost = theta_vacant_posterior[:,0]
Cpost = np.reshape(Cpost, (40000,1))
Hpost = np.reshape(Hpost, (40000,1))
C2post = np.reshape(C2post, (40000,1))
H2post = np.reshape(H2post, (40000,1))
CHpost = np.reshape(CHpost, (40000,1))
Vpost = np.reshape(Vpost, (40000,1))
x1post = np.hstack((Cpost,Hpost))
x2post = np.hstack((x1post,C2post))
x3post = np.hstack((x2post,H2post))
x4post = np.hstack((x3post,CHpost))
xpost = np.hstack((x4post,Vpost))
xpost=np.reshape(xpost, (40000,6))
pickle.dump(xpost, open("PosteriorAdsFrac_HSSZ13_900.p", "wb"))
preset_bins = 10
fig2, ax2 = plt.subplots()
ax2.hist(xpost, bins=preset_bins, label=[r'$\theta_{C2H4}$',r'$\theta_{H2O}$',r'$\theta_{2 C2H4}$',r'$\theta_{2 H2O}$', r'$\theta_{C2H4+H2O}$', r'$\theta_{vacant}$'])
ax2.set_title('Posterior Adsorption Fractions - 900ppm',  fontsize=18)
ax2.set_ylabel('Frequency', fontsize=18)
ax2.set_xlabel('Surface Fraction', fontsize=18)
ax2.set_xlim(-.02,1.02)
ax2.legend(loc='best', fontsize=16)
fig2.savefig('PosteriorAdsFrac900ppm_HSSZ13.png', dpi=220)

Cpost2 = theta_C2H4_posterior[:,1]
C2post2 = theta_2C2H4_posterior[:,1]
Vpost2 = theta_vacant_posterior[:,1]
Hpost2 = theta_H2O_posterior[:,1]
H2post2 = theta_2H2O_posterior[:,1]
CHpost2 = theta_C2H4H2O_posterior[:,1]
Cpost2 = np.reshape(Cpost2, (40000,1))
C2post2 = np.reshape(C2post2, (40000,1))
Vpost2 = np.reshape(Vpost2, (40000,1))
Hpost2 = np.reshape(Hpost2, (40000,1))
H2post2 = np.reshape(H2post2, (40000,1))
CHpost2 = np.reshape(CHpost2, (40000,1))
x1post2 = np.hstack((Cpost2,Hpost2))
x2post2 = np.hstack((x1post2,C2post2))
x3post2 = np.hstack((x2post2,H2post2))
x4post2 = np.hstack((x3post2,CHpost2))
xpost2 = np.hstack((x4post2,Vpost2))
xpost2=np.reshape(xpost2, (40000,6))
pickle.dump(xpost2, open("PosteriorAdsFrac_HSSZ13_600.p", "wb"))
fig3, ax3 = plt.subplots()
ax3.hist(xpost2, bins=preset_bins, label=[r'$\theta_{C2H4}$',r'$\theta_{H2O}$',r'$\theta_{2 C2H4}$',r'$\theta_{2 H2O}$', r'$\theta_{C2H4+H2O}$', r'$\theta_{vacant}$'])
ax3.set_title('Posterior Adsorption Fractions - 600ppm',  fontsize=18)
ax3.set_ylabel('Frequency', fontsize=18)
ax3.set_xlabel('Surface Fraction', fontsize=18)
ax3.set_xlim(-.02,1.02)
ax3.legend(loc='best', fontsize=16)
fig3.savefig('PosteriorAdsFrac600ppm_HSSZ13.png', dpi=220)

Cpost3 = theta_C2H4_posterior[:,2]
C2post3 = theta_2C2H4_posterior[:,2]
Vpost3 = theta_vacant_posterior[:,2]
Hpost3 = theta_H2O_posterior[:,2]
H2post3 = theta_2H2O_posterior[:,2]
CHpost3 = theta_C2H4H2O_posterior[:,2]
Cpost3 = np.reshape(Cpost3, (40000,1))
C2post3 = np.reshape(C2post3, (40000,1))
Vpost3 = np.reshape(Vpost3, (40000,1))
Hpost3 = np.reshape(Hpost3, (40000,1))
H2post3 = np.reshape(H2post3, (40000,1))
CHpost3 = np.reshape(CHpost3, (40000,1))
x1post3 = np.hstack((Cpost3,Hpost3))
x2post3 = np.hstack((x1post3,C2post3))
x3post3 = np.hstack((x2post3,H2post3))
x4post3 = np.hstack((x3post3,CHpost3))
xpost3 = np.hstack((x4post3,Vpost3))
xpost3=np.reshape(xpost3, (40000,6))
pickle.dump(xpost3, open("PosteriorAdsFrac_HSSZ13_450.p", "wb"))
fig4, ax4 = plt.subplots()
ax4.hist(xpost3, bins=preset_bins, label=[r'$\theta_{C2H4}$',r'$\theta_{H2O}$',r'$\theta_{2 C2H4}$',r'$\theta_{2 H2O}$', r'$\theta_{C2H4+H2O}$', r'$\theta_{vacant}$'])
ax4.set_title('Posterior Adsorption Fractions - 450ppm',  fontsize=18)
ax4.set_ylabel('Frequency', fontsize=18)
ax4.set_xlabel('Surface Fraction', fontsize=18)
ax4.set_xlim(-.02,1.02)
ax4.legend(loc='best', fontsize=16)
fig4.savefig('PosteriorAdsFrac450ppm_HSSZ13.png', dpi=220)

Cpost4 = theta_C2H4_posterior[:,3]
C2post4 = theta_2C2H4_posterior[:,3]
Vpost4 = theta_vacant_posterior[:,3]
Hpost4 = theta_H2O_posterior[:,3]
H2post4 = theta_2H2O_posterior[:,3]
CHpost4 = theta_C2H4H2O_posterior[:,3]
Cpost4 = np.reshape(Cpost4, (40000,1))
C2post4 = np.reshape(C2post4, (40000,1))
Vpost4 = np.reshape(Vpost4, (40000,1))
Hpost4 = np.reshape(Hpost4, (40000,1))
H2post4 = np.reshape(H2post4, (40000,1))
CHpost4 = np.reshape(CHpost4, (40000,1))
x1post4 = np.hstack((Cpost4,Hpost4))
x2post4 = np.hstack((x1post4,C2post4))
x3post4 = np.hstack((x2post4,H2post4))
x4post4 = np.hstack((x3post4,CHpost4))
xpost4 = np.hstack((x4post4,Vpost4))
xpost4=np.reshape(xpost4, (40000,6))
pickle.dump(xpost4, open("PosteriorAdsFrac_HSSZ13_300.p", "wb"))
fig5, ax5 = plt.subplots()
ax5.hist(xpost4, bins=preset_bins, label=[r'$\theta_{C2H4}$',r'$\theta_{H2O}$',r'$\theta_{2 C2H4}$',r'$\theta_{2 H2O}$', r'$\theta_{C2H4+H2O}$', r'$\theta_{vacant}$'])
ax5.set_title('Posterior Adsorption Fractions - 300ppm',  fontsize=18)
ax5.set_ylabel('Frequency', fontsize=18)
ax5.set_xlabel('Surface Fraction', fontsize=18)
ax5.set_xlim(-.02,1.02)
ax5.legend(loc='best', fontsize=16)
fig5.savefig('PosteriorAdsFrac300ppm_HSSZ13.png', dpi=220)

Cpost5 = theta_C2H4_posterior[:,4]
C2post5 = theta_2C2H4_posterior[:,4]
Vpost5 = theta_vacant_posterior[:,4]
Hpost5 = theta_H2O_posterior[:,4]
H2post5 = theta_2H2O_posterior[:,4]
CHpost5 = theta_C2H4H2O_posterior[:,4]
Cpost5 = np.reshape(Cpost5, (40000,1))
C2post5 = np.reshape(C2post5, (40000,1))
Vpost5 = np.reshape(Vpost5, (40000,1))
Hpost5 = np.reshape(Hpost5, (40000,1))
H2post5 = np.reshape(H2post5, (40000,1))
CHpost5 = np.reshape(CHpost5, (40000,1))
x1post5 = np.hstack((Cpost5,Hpost5))
x2post5 = np.hstack((x1post5,C2post5))
x3post5 = np.hstack((x2post5,H2post5))
x4post5 = np.hstack((x3post5,CHpost5))
xpost5 = np.hstack((x4post5,Vpost5))
xpost5=np.reshape(xpost5, (40000,6))
pickle.dump(xpost5, open("PosteriorAdsFrac_HSSZ13_200.p", "wb"))
fig6, ax6 = plt.subplots()
ax6.hist(xpost5, bins=preset_bins, label=[r'$\theta_{C2H4}$',r'$\theta_{H2O}$',r'$\theta_{2 C2H4}$',r'$\theta_{2 H2O}$', r'$\theta_{C2H4+H2O}$', r'$\theta_{vacant}$'])
ax6.set_title('Posterior Adsorption Fractions - 200ppm',  fontsize=18)
ax6.set_ylabel('Frequency', fontsize=18)
ax6.set_xlabel('Surface Fraction', fontsize=18)
ax6.set_xlim(-.02,1.02)
ax6.legend(loc='best', fontsize=16)
fig6.savefig('PosteriorAdsFrac200ppm_HSSZ13.png', dpi=220)

