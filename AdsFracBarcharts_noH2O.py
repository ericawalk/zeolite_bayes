#!/usr/bin/env python
# coding: utf-8
# In[1]:
import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import scipy
from scipy.stats import multivariate_normal
from scipy.integrate import odeint
import pandas as pd
import seaborn as sns
import pickle

x = pickle.load(open("NEWPriorAdsFrac_noH2O_900.p", "rb"))
x2 = pickle.load(open("NEWPriorAdsFrac_noH2O_600.p", "rb"))
x3 = pickle.load(open("NewPriorAdsFrac_noH2O_450.p", "rb"))
x4 = pickle.load(open("NEWPriorAdsFrac_noH2O_300.p", "rb"))
x5 = pickle.load(open("NEWPriorAdsFrac_noH2O_200.p", "rb"))

xpost = pickle.load(open("NEWPosteriorAdsFrac_noH2O_900.p", "rb"))
xpost2 = pickle.load(open("NEWPosteriorAdsFrac_noH2O_600.p", "rb"))
xpost3 = pickle.load(open("NEWPosteriorAdsFrac_noH2O_450.p", "rb"))
xpost4 = pickle.load(open("NEWPosteriorAdsFrac_noH2O_300.p", "rb"))
xpost5 = pickle.load(open("NEWPosteriorAdsFrac_noH2O_200.p", "rb"))

#------------------------------------------------------------------------------------
#Prior Stacked Bar Charts
N=2000
r=np.arange(N)
barwidth=2

AF_p1 = pd.DataFrame(x, columns=['C2H4_fractions','twoC2H4_fractions','vacant_fractions'])
AF_p1 = AF_p1.sort_values(by = 'C2H4_fractions')
print(AF_p1.shape)
totals_p1 = [i+j+k for i,j,k in zip(AF_p1['C2H4_fractions'], AF_p1['twoC2H4_fractions'], AF_p1['vacant_fractions'])]
Theta_C2H4_p1 = [i / j * 100 for i,j in zip(AF_p1['C2H4_fractions'], totals_p1)]
Theta_2C2H4_p1 = [i / j * 100 for i,j in zip(AF_p1['twoC2H4_fractions'], totals_p1)]
Theta_vacant_p1 = [i / j * 100 for i,j in zip(AF_p1['vacant_fractions'], totals_p1)]
fig_bcp1,ax_bcp1 = plt.subplots()
plt.bar(r,Theta_C2H4_p1, color='b', width=barwidth, label=r'$\theta_{C2H4}$')
plt.bar(r,Theta_2C2H4_p1, bottom=Theta_C2H4_p1, color='r', width=barwidth, label=r'$\theta_{2C2H4}$')
plt.bar(r,Theta_vacant_p1, bottom=[i+j for i,j in zip(Theta_2C2H4_p1,Theta_C2H4_p1)], color='k', width=barwidth, label=r'$\theta_{vacant}$')
ax_bcp1.set_xlabel('Count', fontsize=20)
ax_bcp1.set_ylabel('Adsorption Site Coverage (%)', fontsize=20)
ax_bcp1.set_title('Prior Adsorption Fractions - 900ppm',  fontsize=18)
fig_bcp1.savefig('NEWPriorAdsFrac900ppm-noH2O_barchart.png', dpi=220)
AF_p2 = pd.DataFrame(x2, columns=['C2H4_fractions','twoC2H4_fractions','vacant_fractions'])
AF_p2 = AF_p2.sort_values(by = 'C2H4_fractions')
totals_p2 = [h+i+j for h,i,j in zip(AF_p2['C2H4_fractions'], AF_p2['twoC2H4_fractions'], AF_p2['vacant_fractions'])]
Theta_C2H4_p2 = [i / j * 100 for i,j in zip(AF_p2['C2H4_fractions'], totals_p2)]
Theta_2C2H4_p2 = [i / j * 100 for i,j in zip(AF_p2['twoC2H4_fractions'], totals_p2)]
Theta_vacant_p2 = [i / j * 100 for i,j in zip(AF_p2['vacant_fractions'], totals_p2)]
fig_bcp2,ax_bcp2 = plt.subplots()
plt.bar(r,Theta_C2H4_p2, color='b', width=barwidth, label=r'$\theta_{C2H4}$')
plt.bar(r,Theta_2C2H4_p2, bottom=Theta_C2H4_p2, color='r', width=barwidth, label=r'$\theta_{2C2H4}$')
plt.bar(r,Theta_vacant_p2, bottom=[i+j for i,j in zip(Theta_2C2H4_p2,Theta_C2H4_p2)], color='k', width=barwidth, label=r'$\theta_{vacant}$')
ax_bcp2.set_xlabel('Count', fontsize=20)
ax_bcp2.set_ylabel('Adsorption Site Coverage (%)', fontsize=20)
ax_bcp2.set_title('Prior Adsorption Fractions - 600ppm',  fontsize=18)
fig_bcp2.savefig('NEWPriorAdsFrac600ppm-noH2O_barchart.png', dpi=220)
AF_p3 = pd.DataFrame(x3, columns=['C2H4_fractions','twoC2H4_fractions','vacant_fractions'])
AF_p3 = AF_p3.sort_values(by = 'C2H4_fractions')
totals_p3 = [h+i+j for h,i,j in zip(AF_p3['C2H4_fractions'], AF_p3['twoC2H4_fractions'], AF_p3['vacant_fractions'])]
Theta_C2H4_p3 = [i / j * 100 for i,j in zip(AF_p3['C2H4_fractions'], totals_p3)]
Theta_2C2H4_p3 = [i / j * 100 for i,j in zip(AF_p3['twoC2H4_fractions'], totals_p3)]
Theta_vacant_p3 = [i / j * 100 for i,j in zip(AF_p3['vacant_fractions'], totals_p3)]
fig_bcp3,ax_bcp3 = plt.subplots()
plt.bar(r,Theta_C2H4_p3, color='b', width=barwidth, label=r'$\theta_{C2H4}$')
plt.bar(r,Theta_2C2H4_p3, bottom=Theta_C2H4_p3, color='r', width=barwidth, label=r'$\theta_{2C2H4}$')
plt.bar(r,Theta_vacant_p3, bottom=[i+j for i,j in zip(Theta_2C2H4_p3,Theta_C2H4_p3)], color='k', width=barwidth, label=r'$\theta_{vacant}$')
ax_bcp3.set_xlabel('Count', fontsize=20)
ax_bcp3.set_ylabel('Adsorption Site Coverage (%)', fontsize=20)
ax_bcp3.set_title('Prior Adsorption Fractions - 450ppm',  fontsize=18)
fig_bcp3.savefig('NEWPriorAdsFrac450ppm-noH2O_barchart.png', dpi=220)
AF_p4 = pd.DataFrame(x4, columns=['C2H4_fractions','twoC2H4_fractions','vacant_fractions'])
AF_p4 = AF_p4.sort_values(by = 'C2H4_fractions')
totals_p4 = [h+i+j for h,i,j in zip(AF_p4['C2H4_fractions'], AF_p4['twoC2H4_fractions'], AF_p4['vacant_fractions'])]
Theta_C2H4_p4 = [i / j * 100 for i,j in zip(AF_p4['C2H4_fractions'], totals_p4)]
Theta_2C2H4_p4 = [i / j * 100 for i,j in zip(AF_p4['twoC2H4_fractions'], totals_p4)]
Theta_vacant_p4 = [i / j * 100 for i,j in zip(AF_p4['vacant_fractions'], totals_p4)]
fig_bcp4,ax_bcp4 = plt.subplots()
plt.bar(r,Theta_C2H4_p4, color='b', width=barwidth, label=r'$\theta_{C2H4}$')
plt.bar(r,Theta_2C2H4_p4, bottom=Theta_C2H4_p4, color='r', width=barwidth, label=r'$\theta_{2C2H4}$')
plt.bar(r,Theta_vacant_p4, bottom=[i+j for i,j in zip(Theta_2C2H4_p4,Theta_C2H4_p4)], color='k', width=barwidth, label=r'$\theta_{vacant}$')
ax_bcp4.set_xlabel('Count', fontsize=20)
ax_bcp4.set_ylabel('Adsorption Site Coverage (%)', fontsize=20)
ax_bcp4.set_title('Prior Adsorption Fractions - 300ppm',  fontsize=18)
fig_bcp4.savefig('NEWPriorAdsFrac300ppm-noH2O_barchart.png', dpi=220)
AF_p5 = pd.DataFrame(x5, columns=['C2H4_fractions','twoC2H4_fractions','vacant_fractions'])
AF_p5 = AF_p5.sort_values(by = 'C2H4_fractions')
totals_p5 = [h+i+j for h,i,j in zip(AF_p5['C2H4_fractions'], AF_p5['twoC2H4_fractions'], AF_p5['vacant_fractions'])]
Theta_C2H4_p5 = [i / j * 100 for i,j in zip(AF_p5['C2H4_fractions'], totals_p5)]
Theta_2C2H4_p5 = [i / j * 100 for i,j in zip(AF_p5['twoC2H4_fractions'], totals_p5)]
Theta_vacant_p5 = [i / j * 100 for i,j in zip(AF_p5['vacant_fractions'], totals_p5)]
fig_bcp5,ax_bcp5 = plt.subplots()
plt.bar(r,Theta_C2H4_p5, color='b', width=barwidth, label=r'$\theta_{C2H4}$')
plt.bar(r,Theta_2C2H4_p5, bottom=Theta_C2H4_p5, color='r', width=barwidth, label=r'$\theta_{2C2H4}$')
plt.bar(r,Theta_vacant_p5, bottom=[i+j for i,j in zip(Theta_2C2H4_p5,Theta_C2H4_p5)], color='k', width=barwidth, label=r'$\theta_{vacant}$')
ax_bcp5.set_xlabel('Count', fontsize=20)
ax_bcp5.set_ylabel('Adsorption Site Coverage (%)', fontsize=20)
ax_bcp5.set_title('Prior Adsorption Fractions - 200ppm',  fontsize=18)
fig_bcp5.savefig('NEWPriorAdsFrac200ppm-noH2O_barchart.png', dpi=220)

#------------------------------------------------------------------
#Posterior Stacked Bar Charts

AF_post1 = pd.DataFrame(xpost, columns=['C2H4_fractions','twoC2H4_fractions','vacant_fractions'])
AF_post1 = AF_post1.sort_values(by = 'C2H4_fractions')
totals_post1 = [h+i+j for h,i,j in zip(AF_post1['C2H4_fractions'], AF_post1['twoC2H4_fractions'], AF_post1['vacant_fractions'])]
Theta_C2H4_post1 = [i / j * 100 for i,j in zip(AF_post1['C2H4_fractions'], totals_post1)]
Theta_2C2H4_post1 = [i / j * 100 for i,j in zip(AF_post1['twoC2H4_fractions'], totals_post1)]
Theta_vacant_post1 = [i / j * 100 for i,j in zip(AF_post1['vacant_fractions'], totals_post1)]
fig_bc1,ax_bc1 = plt.subplots()
plt.bar(r,Theta_C2H4_post1, color='b', width=barwidth, label=r'$\theta_{C2H4}$')
plt.bar(r,Theta_2C2H4_post1, bottom=Theta_C2H4_post1, color='r', width=barwidth, label=r'$\theta_{2C2H4}$')
plt.bar(r,Theta_vacant_post1, bottom=[i+j for i,j in zip(Theta_2C2H4_post1,Theta_C2H4_post1)], color='k', width=barwidth, label=r'$\theta_{vacant}$')
ax_bc1.set_xlabel('Count', fontsize=20)
ax_bc1.set_ylabel('Adsorption Site Coverage (%)', fontsize=20)
ax_bc1.set_title('Posterior Adsorption Fractions - 900ppm',  fontsize=18)
fig_bc1.savefig('NEWPosteriorAdsFrac900ppm-noH2O_barchart.png', dpi=220)
AF_post2 = pd.DataFrame(xpost2, columns=['C2H4_fractions','twoC2H4_fractions','vacant_fractions'])
AF_post2 = AF_post2.sort_values(by = 'C2H4_fractions')
totals_post2 = [h+i+j for h,i,j in zip(AF_post2['C2H4_fractions'], AF_post2['twoC2H4_fractions'], AF_post2['vacant_fractions'])]
Theta_C2H4_post2 = [i / j * 100 for i,j in zip(AF_post2['C2H4_fractions'], totals_post2)]
Theta_2C2H4_post2 = [i / j * 100 for i,j in zip(AF_post2['twoC2H4_fractions'], totals_post2)]
Theta_vacant_post2 = [i / j * 100 for i,j in zip(AF_post2['vacant_fractions'], totals_post2)]
fig_bc2,ax_bc2 = plt.subplots()
plt.bar(r,Theta_C2H4_post2, color='b', width=barwidth, label=r'$\theta_{C2H4}$')
plt.bar(r,Theta_2C2H4_post2, bottom=Theta_C2H4_post2, color='r', width=barwidth, label=r'$\theta_{2C2H4}$')
plt.bar(r,Theta_vacant_post2, bottom=[i+j for i,j in zip(Theta_2C2H4_post2,Theta_C2H4_post2)], color='k', width=barwidth, label=r'$\theta_{vacant}$')
ax_bc2.set_xlabel('Count', fontsize=20)
ax_bc2.set_ylabel('Adsorption Site Coverage (%)', fontsize=20)
ax_bc2.set_title('Posterior Adsorption Fractions - 600ppm',  fontsize=18)
fig_bc2.savefig('NEWPosteriorAdsFrac600ppm-noH2O_barchart.png', dpi=220)
AF_post3 = pd.DataFrame(xpost3, columns=['C2H4_fractions','twoC2H4_fractions','vacant_fractions'])
AF_post3 = AF_post3.sort_values(by = 'C2H4_fractions')
totals_post3 = [h+i+j for h,i,j in zip(AF_post3['C2H4_fractions'], AF_post3['twoC2H4_fractions'], AF_post3['vacant_fractions'])]
Theta_C2H4_post3 = [i / j * 100 for i,j in zip(AF_post3['C2H4_fractions'], totals_post3)]
Theta_2C2H4_post3 = [i / j * 100 for i,j in zip(AF_post3['twoC2H4_fractions'], totals_post3)]
Theta_vacant_post3 = [i / j * 100 for i,j in zip(AF_post3['vacant_fractions'], totals_post3)]
fig_bc3,ax_bc3 = plt.subplots()
plt.bar(r,Theta_C2H4_post3, color='b', width=barwidth, label=r'$\theta_{C2H4}$')
plt.bar(r,Theta_2C2H4_post3, bottom=Theta_C2H4_post3, color='r', width=barwidth, label=r'$\theta_{2C2H4}$')
plt.bar(r,Theta_vacant_post3, bottom=[i+j for i,j in zip(Theta_2C2H4_post3,Theta_C2H4_post3)], color='k', width=barwidth, label=r'$\theta_{vacant}$')
ax_bc3.set_xlabel('Count', fontsize=20)
ax_bc3.set_ylabel('Adsorption Site Coverage (%)', fontsize=20)
ax_bc3.set_title('Posterior Adsorption Fractions - 450ppm',  fontsize=18)
fig_bc3.savefig('NEWPosteriorAdsFrac450ppm-noH2O_barchart.png', dpi=220)
AF_post4 = pd.DataFrame(xpost4, columns=['C2H4_fractions','twoC2H4_fractions','vacant_fractions'])
AF_post4 = AF_post4.sort_values(by = 'C2H4_fractions')
totals_post4 = [h+i+j for h,i,j in zip(AF_post4['C2H4_fractions'], AF_post4['twoC2H4_fractions'], AF_post4['vacant_fractions'])]
Theta_C2H4_post4 = [i / j * 100 for i,j in zip(AF_post4['C2H4_fractions'], totals_post4)]
Theta_2C2H4_post4 = [i / j * 100 for i,j in zip(AF_post4['twoC2H4_fractions'], totals_post4)]
Theta_vacant_post4 = [i / j * 100 for i,j in zip(AF_post4['vacant_fractions'], totals_post4)]
fig_bc4,ax_bc4 = plt.subplots()
plt.bar(r,Theta_C2H4_post4, color='b', width=barwidth, label=r'$\theta_{C2H4}$')
plt.bar(r,Theta_2C2H4_post4, bottom=Theta_C2H4_post4, color='r', width=barwidth, label=r'$\theta_{2C2H4}$')
plt.bar(r,Theta_vacant_post4, bottom=[i+j for i,j in zip(Theta_2C2H4_post4,Theta_C2H4_post4)], color='k', width=barwidth, label=r'$\theta_{vacant}$')
ax_bc4.set_xlabel('Count', fontsize=20)
ax_bc4.set_ylabel('Adsorption Site Coverage (%)', fontsize=20)
ax_bc4.set_title('Posterior Adsorption Fractions - 300ppm',  fontsize=18)
fig_bc4.savefig('NEWPosteriorAdsFrac300ppm-noH2O_barchart.png', dpi=220)
AF_post5 = pd.DataFrame(xpost5, columns=['C2H4_fractions','twoC2H4_fractions','vacant_fractions'])
AF_post5 = AF_post5.sort_values(by = 'C2H4_fractions')
totals_post5 = [h+i+j for h,i,j in zip(AF_post5['C2H4_fractions'], AF_post5['twoC2H4_fractions'], AF_post5['vacant_fractions'])]
Theta_C2H4_post5 = [i / j * 100 for i,j in zip(AF_post5['C2H4_fractions'], totals_post5)]
Theta_2C2H4_post5 = [i / j * 100 for i,j in zip(AF_post5['twoC2H4_fractions'], totals_post5)]
Theta_vacant_post5 = [i / j * 100 for i,j in zip(AF_post5['vacant_fractions'], totals_post5)]
fig_bc5,ax_bc5 = plt.subplots()
plt.bar(r,Theta_C2H4_post5, color='b', width=barwidth, label=r'$\theta_{C2H4}$')
plt.bar(r,Theta_2C2H4_post5, bottom=Theta_C2H4_post5, color='r', width=barwidth, label=r'$\theta_{2C2H4}$')
plt.bar(r,Theta_vacant_post5, bottom=[i+j for i,j in zip(Theta_2C2H4_post5,Theta_C2H4_post5)], color='k', width=barwidth, label=r'$\theta_{vacant}$')
ax_bc5.set_xlabel('Count', fontsize=20)
ax_bc5.set_ylabel('Adsorption Site Coverage (%)', fontsize=20)
ax_bc5.set_title('Posterior Adsorption Fractions - 200ppm',  fontsize=18)
fig_bc5.savefig('NEWPosteriorAdsFrac200ppm-noH2O_barchart.png', dpi=220)



