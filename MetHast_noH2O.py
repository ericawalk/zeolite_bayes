#!/usr/bin/env python
# coding: utf-8

# In[1]:

import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D 
import scipy
from scipy.stats import multivariate_normal
from scipy.integrate import odeint
import pandas as pd
import seaborn as sns
import pickle

class ip:
    mcmc_length = 50000  
    mcmc_burn_in = 10000  
    mu_prior = np.array([-0.6075516799917321, -0.5623760389378045])
    cov_prior = np.array([[0.10159975, 0.11758658],
                          [0.11758658, 0.19251888]])
    experiment = np.array([1, 0.926, 0.889, 0.889, 0.852])   #5 long
    Q_mu = np.array([0,0])
    Q_cov = cov_prior
    #main function to get samples
    def MetropolisHastings(self):
        samples = np.zeros((self.mcmc_length,2)) 
        samples[0,:] = self.mu_prior #initialize the chain
        likelihoods_vec = np.zeros((self.mcmc_length,5))
        posteriors_un_normed_vec = np.zeros((self.mcmc_length,1))
        priors_vec = np.zeros((self.mcmc_length,1))
        for i in range(1,self.mcmc_length):
            proposal_sample = samples[i-1,:] + np.random.multivariate_normal(self.Q_mu,self.Q_cov)
            prior_proposal = self.prior(proposal_sample)
            likelihood_proposal = self.likelihood(proposal_sample)
            prior_current_location = self.prior(samples[i-1,:])
            likelihood_current_location = self.likelihood(samples[i-1,:])
            accept_pro = (likelihood_proposal*prior_proposal)/(likelihood_current_location*prior_current_location)
            uni_rand = np.random.uniform()
            if uni_rand<accept_pro:
                samples[i,:] = proposal_sample
                posteriors_un_normed_vec[i] = likelihood_proposal*prior_proposal
                likelihoods_vec[i] = likelihood_proposal
                priors_vec[i] = prior_proposal
            else:
                samples[i,:] = samples[i-1,:]
                posteriors_un_normed_vec[i] = likelihood_current_location*prior_current_location
                likelihoods_vec[i] = likelihood_current_location
                priors_vec[i] = prior_current_location
            ########################################
        samples = samples[self.mcmc_burn_in:]
        posteriors_un_normed_vec = posteriors_un_normed_vec[self.mcmc_burn_in:]
        likelihoods_vec = likelihoods_vec[self.mcmc_burn_in:]
        priors_vec = priors_vec[self.mcmc_burn_in:]
        #posterior probabilites are transformed to a standard normal (std=1)
        #for obtaining the evidence:
        evidence = np.mean(posteriors_un_normed_vec)*np.sqrt(2*np.pi*np.std(samples)**2)
        posteriors_vec = posteriors_un_normed_vec/evidence
        log_ratios = np.log(posteriors_vec/priors_vec)
        log_ratios[np.isinf(log_ratios)] = 0
        log_ratios = np.nan_to_num(log_ratios)
        info_gain = np.mean(log_ratios)
        return [evidence, info_gain, samples]
    
    def prior(self,sample):
        probability = multivariate_normal.pdf(x=sample, mean=self.mu_prior, cov=self.cov_prior, allow_singular=False)
        return probability
        
    def likelihood(self,sample):
        c2h4_totalarray = np.zeros(5)
        c2h4_adsarray = np.zeros(5)
        for j,C in enumerate(np.array(concentrations)):
            sol = odeint(microkinetic_model, y0, t, args=(C, f1, sample[0], sample[1], ))
            c2h4_1Frac = sol[-1,0]
            c2h4_2Frac = sol[-1,1]
            c2h4_2Frac2 = 2.0*c2h4_2Frac
            c2h4_total = c2h4_1Frac+c2h4_2Frac2
            c2h4_totalarray[j] = c2h4_total
            print(C)
            print(c2h4_totalarray)
        c2h4_adsarray = c2h4_totalarray[:]/c2h4_totalarray[0]  
        probability_ra = multivariate_normal.pdf(x=c2h4_adsarray,mean=experiment, cov=0.005)
        return probability_ra
#-------------------------------------------------------------------------------------------
#     Microkinetic Model Function
concentrations = np.array([9e-4, 6e-4, 4.5e-4, 3e-4, 2e-4])
experiment = np.array([1, 0.926, 0.889, 0.889, 0.852])
y0 = y=np.array([0, 0, 1])
t = np.linspace(0,1E10,1E4)
f1 = 6.2014895E5 #adsorption rate constant ethylene on zeolite from collision theory
kB = 8.617E-5
T=298.15 
def microkinetic_model(y, t, C, f1, G_1, G_4):
        theta_C2H4, theta_2C2H4, theta_vacant = y
        K_1 = np.exp(-(G_1)/(kB*T))
        K_4 = np.exp(-(G_4)/(kB*T))
        kr1 = f1/K_1
        kr4 = f1/K_4       #ethylene adsorbed, C2H4 already adsorbed 
        r_rxn1 = f1*theta_vacant*C - kr1*theta_C2H4
        r_rxn4 = f1*theta_vacant*theta_C2H4*C - kr4*theta_2C2H4
        d_theta_C2H4_dt = r_rxn1 - r_rxn4
        d_theta_2C2H4_dt = r_rxn4
        d_theta_vacant_dt = -r_rxn1 - r_rxn4
        dydt = [d_theta_C2H4_dt, d_theta_2C2H4_dt, d_theta_vacant_dt]
        return dydt
#----------------------------------------------------------------------------------
#--------------------Finding Priors and Posteriors---------------------
#-----------------------------------------------------------------------------------------------
#calculating prior adsorption fractions
mu_prior = np.array([-0.6075516799917321, -0.5623760389378045])
cov_prior = np.array([[0.10159975, 0.11758658],
                      [0.11758658, 0.19251888]])
G_prior=np.random.multivariate_normal(mu_prior, cov_prior, 40000)
theta_C2H4_prior=np.zeros((40000, 5))
theta_2C2H4_prior=np.zeros((40000,5))
theta_vacant_prior=np.zeros((40000,5))
for j,C in enumerate(np.array([9e-4, 6e-4, 4.5e-4, 3e-4, 2e-4])):
    print('concentration: ',C)
    for i in range(G_prior.shape[0]):    #shape[0] is 2000
        sol = odeint(microkinetic_model, y0, t, args=(C, f1, G_prior[i,0], G_prior[i,1]))
        theta_C2H4_prior[i,j] = sol[-1,0]
        theta_2C2H4_prior[i,j] = sol[-1,1]
        theta_vacant_prior[i,j] = sol[-1,2]
#graphing prior ads frac when specific concentration
Cp = theta_C2H4_prior[:,0]
C2p = theta_2C2H4_prior[:,0]
Vp = theta_vacant_prior[:,0]
Cp = np.reshape(Cp, (40000,1))
C2p = np.reshape(C2p, (40000,1))
Vp = np.reshape(Vp, (40000,1))
x1 = np.hstack((Cp,C2p))
x = np.hstack((x1,Vp))
x=np.reshape(x, (40000,3))
pickle.dump(x, open("NEWPriorAdsFrac_noH2O_900.p", "wb"))
Cp2 = theta_C2H4_prior[:,1]
C2p2 = theta_2C2H4_prior[:,1]
Vp2 = theta_vacant_prior[:,1]
Cp2 = np.reshape(Cp2, (40000,1))
C2p2 = np.reshape(C2p2, (40000,1))
Vp2 = np.reshape(Vp2, (40000,1))
x12 = np.hstack((Cp2,C2p2))
x2 = np.hstack((x12,Vp2))
x2=np.reshape(x2, (40000,3))
pickle.dump(x2, open("NEWPriorAdsFrac_noH2O_600.p", "wb"))
Cp3 = theta_C2H4_prior[:,2]
C2p3 = theta_2C2H4_prior[:,2]
Vp3 = theta_vacant_prior[:,2]
Cp3 = np.reshape(Cp3, (40000,1))
C2p3 = np.reshape(C2p3, (40000,1))
Vp3 = np.reshape(Vp3, (40000,1))
x13 = np.hstack((Cp3,C2p3))
x3 = np.hstack((x13,Vp3))
x3=np.reshape(x3, (40000,3))
pickle.dump(x3, open("NewPriorAdsFrac_noH2O_450.p", "wb"))
Cp4 = theta_C2H4_prior[:,3]
C2p4 = theta_2C2H4_prior[:,3]
Vp4 = theta_vacant_prior[:,3]
Cp4 = np.reshape(Cp4, (40000,1))
C2p4 = np.reshape(C2p4, (40000,1))
Vp4 = np.reshape(Vp4, (40000,1))
x14 = np.hstack((Cp4,C2p4))
x4 = np.hstack((x14,Vp4))
x4=np.reshape(x4, (40000,3))
pickle.dump(x4, open("NEWPriorAdsFrac_noH2O_300.p", "wb"))
Cp5 = theta_C2H4_prior[:,4]
C2p5 = theta_2C2H4_prior[:,4]
Vp5 = theta_vacant_prior[:,4]
Cp5 = np.reshape(Cp5, (40000,1))
C2p5 = np.reshape(C2p5, (40000,1))
Vp5 = np.reshape(Vp5, (40000,1))
x15 = np.hstack((Cp5,C2p5))
x5 = np.hstack((x15,Vp5))
x5=np.reshape(x5, (40000,3))
pickle.dump(x5, open("NEWPriorAdsFrac_noH2O_200.p", "wb"))
preset_bins = 10
fig, ax = plt.subplots()
ax.hist(x, bins=preset_bins, label=[r'$\theta_{C2H4}$',r' $\theta_{2 C2H4}$', r'$\theta_{vacant}$'])
ax.set_title('Prior Adsorption Fractions - 900ppm',  fontsize=18)
ax.set_ylabel('Frequency', fontsize=18)
ax.set_xlabel('Surface Fraction', fontsize=18)
ax.set_xlim(-.02,1.02)
ax.set_ylim(0,2000)
ax.legend(loc='best', fontsize=16)
fig.savefig('NEWPriorAdsFrac900ppm-noH2O.png', dpi=220)

#----------------------------------------------------------------------------------------------------
#calculating posterior adsorption fractions  

evidences=[]
info_gains=[]
all_samples=[]
ip_object = ip()
concentrations = np.array([9e-4, 6e-4, 4.5e-4, 3e-4, 2e-4])
experiment = np.array([1, 0.926, 0.889, 0.889, 0.852])
[evidence, info_gain, samples] = ip_object.MetropolisHastings()
evidences.append(evidence)
info_gains.append(info_gain)
all_samples.append(samples)
print('Evidence: ' + str(evidence))
print('Information Gain: ' + str(info_gain))
print('Samples: ' +str(samples))
print(samples.shape)  
print(evidence.shape)
# One ethylene, One water, [Water to Ethylene Ads], Two ethylene, Two water, Ethylene to Water Ads
samplearray = np.asarray(all_samples)
print('all samples shape')
print(samplearray.shape)     #(1, 40000, 5)
samplearray = np.reshape(samplearray, (40000,2))
evidencearray = np.asarray(evidences)
print('evidence shape')
print(evidencearray.shape)
np.savetxt('Evidence.csv', evidencearray)


prior_c2h4 = G_prior[:,0]
prior_2c2h4 = G_prior[:,1]
sample_c2h4 = samplearray[:,0]
sample_2c2h4 = samplearray[:,1]
fig_s, ax_s = plt.subplots()
ax_s.hist(prior_c2h4, bins=30, alpha=0.5, label='prior')
ax_s.hist(sample_c2h4, bins=30, alpha=0.5, label='posterior')
ax_s.set_title('Prior and Posterior C2H4 Energy',  fontsize=18)
ax_s.set_ylabel('Frequency', fontsize=18)
ax_s.set_xlabel('Energy', fontsize=18)
ax_s.legend(loc='best', fontsize=16)
fig_s.savefig('NEWAdsEng_PriorPosteriorC2H4.png', dpi=220)
fig_s2, ax_s2 = plt.subplots()
ax_s2.hist(prior_2c2h4, bins=30, alpha=0.5, label='prior')
ax_s2.hist(sample_2c2h4, bins=30, alpha=0.5, label='posterior')
ax_s2.set_title('Prior and Posterior 2 C2H4 Energy',  fontsize=18)
ax_s2.set_ylabel('Frequency', fontsize=18)
ax_s2.set_xlabel('Energy', fontsize=18)
ax_s2.legend(loc='best', fontsize=16)
fig_s2.savefig('NEWAdsEng_PriorPosterior2C2H4.png', dpi=220)
fig_s3, ax_s3 = plt.subplots()

theta_C2H4_posterior=np.zeros((40000, 5))
theta_2C2H4_posterior=np.zeros((40000,5))
theta_vacant_posterior=np.zeros((40000,5))
for j,C in enumerate(np.array([9e-4, 6e-4, 4.5e-4, 3e-4, 2e-4])):
    print('concentration: ',C)
    for i in range(samplearray.shape[0]):    
        sol = odeint(microkinetic_model, y0, t, args=(C, f1, sample_c2h4[i], sample_2c2h4[i]))
        theta_C2H4_posterior[i,j] = sol[-1,0]
        theta_2C2H4_posterior[i,j] = sol[-1,1]
        theta_vacant_posterior[i,j] = sol[-1,2]
#graphing posterior ads frac when specific concentration
Cpost = theta_C2H4_posterior[:,0]
C2post = theta_2C2H4_posterior[:,0]
Vpost = theta_vacant_posterior[:,0]
Cpost = np.reshape(Cpost, (40000,1))
C2post = np.reshape(C2post, (40000,1))
Vpost = np.reshape(Vpost, (40000,1))
x1post = np.hstack((Cpost,C2post))
xpost = np.hstack((x1post,Vpost))
xpost = np.reshape(xpost, (40000,3))
pickle.dump(xpost, open("NEWPosteriorAdsFrac_noH2O_900.p", "wb"))
preset_bins = 10
fig2, ax2 = plt.subplots()
ax2.hist(xpost, bins=preset_bins, label=[r'$\theta_{C2H4}$', r'$\theta_{2 C2H4}$', r'$\theta_{vacant}$'])
ax2.set_title('Posterior Adsorption Fractions - 900ppm',  fontsize=18)
ax2.set_ylabel('Frequency', fontsize=18)
ax2.set_xlabel('Surface Fraction', fontsize=18)
ax2.set_xlim(-.02,1.02)
ax2.legend(loc='best', fontsize=16)
fig2.savefig('NEWPosteriorAdsFrac900ppm-noH2O.png', dpi=220)

Cpost2 = theta_C2H4_posterior[:,1]
C2post2 = theta_2C2H4_posterior[:,1]
Vpost2 = theta_vacant_posterior[:,1]
Cpost2 = np.reshape(Cpost2, (40000,1))
C2post2 = np.reshape(C2post2, (40000,1))
Vpost2 = np.reshape(Vpost2, (40000,1))
x1post2 = np.hstack((Cpost2,C2post2))
xpost2 = np.hstack((x1post2,Vpost2))
xpost2 = np.reshape(xpost2, (40000,3))
pickle.dump(xpost2, open("NEWPosteriorAdsFrac_noH2O_600.p", "wb"))
fig3, ax3 = plt.subplots()
ax3.hist(xpost2, bins=preset_bins, label=[r'$\theta_{C2H4}$', r'$\theta_{2 C2H4}$', r'$\theta_{vacant}$'])
ax3.set_title('Posterior Adsorption Fractions - 600ppm',  fontsize=18)
ax3.set_ylabel('Frequency', fontsize=18)
ax3.set_xlabel('Surface Fraction', fontsize=18)
ax3.set_xlim(-.02,1.02)
ax3.legend(loc='best', fontsize=16)
fig3.savefig('NEWPosteriorAdsFrac600ppm-noH2O.png', dpi=220)

Cpost3 = theta_C2H4_posterior[:,2]
C2post3 = theta_2C2H4_posterior[:,2]
Vpost3 = theta_vacant_posterior[:,2]
Cpost3 = np.reshape(Cpost3, (40000,1))
C2post3 = np.reshape(C2post3, (40000,1))
Vpost3 = np.reshape(Vpost3, (40000,1))
x1post3 = np.hstack((Cpost3,C2post3))
xpost3 = np.hstack((x1post3,Vpost3))
xpost3 = np.reshape(xpost3, (40000,3))
pickle.dump(xpost3, open("NEWPosteriorAdsFrac_noH2O_450.p", "wb"))
fig4, ax4 = plt.subplots()
ax4.hist(xpost3, bins=preset_bins, label=[r'$\theta_{C2H4}$', r'$\theta_{2 C2H4}$', r'$\theta_{vacant}$'])
ax4.set_title('Posterior Adsorption Fractions - 450ppm',  fontsize=18)
ax4.set_ylabel('Frequency', fontsize=18)
ax4.set_xlabel('Surface Fraction', fontsize=18)
ax4.set_xlim(-.02,1.02)
ax4.legend(loc='best', fontsize=16)
fig4.savefig('NEWPosteriorAdsFrac450ppm-noH2O.png', dpi=220)

Cpost4 = theta_C2H4_posterior[:,3]
C2post4 = theta_2C2H4_posterior[:,3]
Vpost4 = theta_vacant_posterior[:,3]
Cpost4 = np.reshape(Cpost4, (40000,1))
C2post4 = np.reshape(C2post4, (40000,1))
Vpost4 = np.reshape(Vpost4, (40000,1))
x1post4 = np.hstack((Cpost4,C2post4))
xpost4 = np.hstack((x1post4,Vpost4))
xpost4 = np.reshape(xpost4, (40000,3))
pickle.dump(xpost4, open("NEWPosteriorAdsFrac_noH2O_300.p", "wb"))
fig5, ax5 = plt.subplots()
ax5.hist(xpost4, bins=preset_bins, label=[r'$\theta_{C2H4}$', r'$\theta_{2 C2H4}$', r'$\theta_{vacant}$'])
ax5.set_title('Posterior Adsorption Fractions - 300ppm',  fontsize=18)
ax5.set_ylabel('Frequency', fontsize=18)
ax5.set_xlabel('Surface Fraction', fontsize=18)
ax5.set_xlim(-.02,1.02)
ax5.legend(loc='best', fontsize=16)
fig5.savefig('NEWPosteriorAdsFrac300ppm-noH2O.png', dpi=220)

Cpost5 = theta_C2H4_posterior[:,4]
C2post5 = theta_2C2H4_posterior[:,4]
Vpost5 = theta_vacant_posterior[:,4]
Cpost5 = np.reshape(Cpost5, (40000,1))
C2post5 = np.reshape(C2post5, (40000,1))
Vpost5 = np.reshape(Vpost5, (40000,1))
x1post5 = np.hstack((Cpost5,C2post5))
xpost5 = np.hstack((x1post5,Vpost5))
xpost5 = np.reshape(xpost5, (40000,3))
pickle.dump(xpost5, open("NEWPosteriorAdsFrac_noH2O_200.p", "wb"))
fig6, ax6 = plt.subplots()
ax6.hist(xpost5, bins=preset_bins, label=[r'$\theta_{C2H4}$', r'$\theta_{2 C2H4}$', r'$\theta_{vacant}$'])
ax6.set_title('Posterior Adsorption Fractions - 200ppm',  fontsize=18)
ax6.set_ylabel('Frequency', fontsize=18)
ax6.set_xlabel('Surface Fraction', fontsize=18)
ax6.set_xlim(-.02,1.02)
ax6.legend(loc='best', fontsize=16)
fig6.savefig('NEWPosteriorAdsFrac200ppm-noH2O.png', dpi=220)



